﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/AdminMaster.master" AutoEventWireup="true" CodeFile="Generatee_Epass.aspx.cs" Inherits="Admin_Generatee_Epass" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     <div class="right_col" role="main">
        <div class="">
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-9 col-sm-12 col-xs-12 col-md-offset-1">
                    <div class="x_panel">
                     <div class="x_title">
                            <h2>Media Center</h2>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <uc1:MyMessageBox ID="MyMessageBox1" runat="server" />
                            <br />
                            <div id="demo-form2" data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">
                               <asp:HiddenField ID="hdnId" runat="server" />

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">
                                        Full Name <span class="required" style="color: red">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <asp:TextBox ID="txtFullName" CssClass="form-control col-md-7 col-xs-12" runat="server"  autocomplete="off"></asp:TextBox>
                                    </div>
                                </div>

                                    <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">
                                        Mobile No <span class="required" style="color: red">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <asp:TextBox ID="txtMobNo" CssClass="form-control col-md-7 col-xs-12" runat="server"  autocomplete="off"></asp:TextBox>
                                    </div>
                                </div>

                                  <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">
                                        Email Id <span class="required" style="color: red">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <asp:TextBox ID="txtEmailId" CssClass="form-control col-md-7 col-xs-12" runat="server"  autocomplete="off"></asp:TextBox>
                                    </div>
                                </div>

                                 <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">
                                        Aadhar Card No <span class="required" style="color: red">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <asp:TextBox ID="txtAdhaar" CssClass="form-control col-md-7 col-xs-12" runat="server"  autocomplete="off"></asp:TextBox>
                                    </div>
                                </div>

                                    <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">
                                        Visitor1 <span class="required" style="color: red">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <asp:TextBox ID="txtVisitor1" CssClass="form-control col-md-7 col-xs-12" runat="server"  autocomplete="off"></asp:TextBox>
                                    </div>
                                </div>

                                   <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">
                                        Visitor1 Aadhaar <span class="required" style="color: red">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <asp:TextBox ID="txtVisitor1_Aadhaar" CssClass="form-control col-md-7 col-xs-12" runat="server" autocomplete="off"></asp:TextBox>
                                    </div>
                                </div>

                                          <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">
                                        Visitor2 <span class="required" style="color: red">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <asp:TextBox ID="txtVisitor2" CssClass="form-control col-md-7 col-xs-12" runat="server"  autocomplete="off"></asp:TextBox>
                                    </div>
                                </div>

                                   <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">
                                        Visitor2 Aadhaar <span class="required" style="color: red">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <asp:TextBox ID="txtVisitor2_Aadhaar" CssClass="form-control col-md-7 col-xs-12" runat="server" autocomplete="off"></asp:TextBox>
                                    </div>
                                </div>

                                     <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">
                                        Visitor3 <span class="required" style="color: red">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <asp:TextBox ID="txtVisitor3" CssClass="form-control col-md-7 col-xs-12" runat="server"  autocomplete="off"></asp:TextBox>
                                    </div>
                                </div>

                                 <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">
                                        Visitor3 Aadhaar <span class="required" style="color: red">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <asp:TextBox ID="txtVisitor3_Aadhaar" CssClass="form-control col-md-7 col-xs-12" runat="server" autocomplete="off"></asp:TextBox>
                                    </div>
                                </div>

                                 <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">
                                        E-pass issued for Date <span class="required" style="color: red">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <asp:TextBox ID="TextBox1" CssClass="form-control col-md-7 col-xs-12" runat="server" autocomplete="off"></asp:TextBox>
                                    </div>
                                </div>
                            
                                </div>

                                <div class="ln_solid"></div>

                                <div class="form-group">
                                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                        <asp:Button ID="btnsumbit" type="Save" Text="Save" value="Submit" class="btn btn-success StoreDDLValueinhihhenfields" title="Submit" runat="server" OnClick="btnsumbit_Click" />
                                        <asp:Button ID="btnreset" value="Cancel" Text="Cancel" class="btn btn-primary" title="Cancel" runat="server" OnClick="btnreset_Click" />
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>
                </div>
            </div>

        </div>


    </div>

</asp:Content>

