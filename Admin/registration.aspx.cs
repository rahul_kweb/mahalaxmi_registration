﻿using ClosedXML.Excel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_registration : AdminPage
{
    Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindAllVisitors();
        }
    }


    public void BindAllVisitors()
    {
        DataSet ds = new DataSet();
        ds = utility.Display1("execute Proc_Register_Applicant 'Get'");
        if (ds.Tables[0].Rows.Count > 0)
        {
            gdView.Columns[0].Visible = false;
            gdView.DataSource = ds;
            gdView.DataBind();
        }
        else
        {
            gdView.DataSource = null;
            gdView.DataBind();
        }
    }


    protected void gdView_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gdView.PageIndex = e.NewPageIndex;
        BindAllVisitors();
    }

    protected void btnAllExport_Click(object sender, EventArgs e)
    {
        using (XLWorkbook wb = new XLWorkbook())
        {
            DataSet ds = new DataSet();
            ds = utility.Display1("Execute Proc_Register_Applicant 'GetForExportToExcelAll'");
            wb.Worksheets.Add(ds);
            wb.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
            wb.Style.Font.Bold = true;

            Response.Clear();
            Response.Buffer = true;
            Response.Charset = "";
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("content-disposition", "attachment;filename= AllRegisterVisitors.xlsx");

            using (MemoryStream MyMemoryStream = new MemoryStream())
            {
                wb.SaveAs(MyMemoryStream);
                MyMemoryStream.WriteTo(Response.OutputStream);
                Response.Flush();
                Response.End();
            }
        }
    }

    protected void gdView_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {

    }

    protected void btnFilterExport_Click(object sender, EventArgs e)
    {
        if (check())
        {

            using (XLWorkbook wb = new XLWorkbook())
            {
                DataSet ds = new DataSet();
                ds = GetDatafromDatabase();
                wb.Worksheets.Add(ds);
                wb.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                wb.Style.Font.Bold = true;

                Response.Clear();
                Response.Buffer = true;
                Response.Charset = "";
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("content-disposition", "attachment;filename = RegisterVisitorsByDateTimeSlot.xlsx");

                using (MemoryStream MyMemoryStream = new MemoryStream())
                {
                    wb.SaveAs(MyMemoryStream);
                    MyMemoryStream.WriteTo(Response.OutputStream);
                    Response.Flush();
                    Response.End();
                }

            }
        }
    }


    protected DataSet GetDatafromDatabase()
    {
        DataSet ds = new DataSet();
        string Date = DateTime.ParseExact(txtdate.Text, "dd-MM-yyyy", new CultureInfo("hi-IN")).ToString("dddd, MMMM d, yyyy");

       
        string TimeSlot = ddlTimeSlot.SelectedItem.Text;

        ds = utility.Display1("execute Proc_Register_Applicant 'GetForExportToExcelFilter',0,'','','','','"+ Date + "','"+ TimeSlot + "'");
        return ds;
    }

    public bool check()
    {
        bool isOK = true;
        string message = string.Empty;
        if (txtdate.Text.Trim().Equals(string.Empty))
        {
            isOK = false;
            message = "Date, ";
        }

        if (ddlTimeSlot.SelectedItem.Text== "Select Time")
        {
            isOK = false;
            message += "Time Slot, ";
        }

        if (message.Length > 0)
        {
            message = message.Substring(0, message.Length - 2);
        }
        if (!isOK)
        {
            MyMessageBox1.ShowError("Please fill following fields <br />" + message);
        }
        return isOK;
    }
}