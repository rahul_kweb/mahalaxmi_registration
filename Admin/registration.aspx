﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/AdminMaster.master" AutoEventWireup="true" CodeFile="registration.aspx.cs" Inherits="Admin_registration" %>

<%@ Register Src="~/Admin/MyMessageBox.ascx" TagName="MyMessageBox" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
        <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>Visitors</h3>
                </div>

                 <div class="title_right">
                    <div class="col-xs-12 form-group text-right ">
                        <asp:Button ID="btnAllExport" Text="Export All to Excel" runat="server" CssClass="fa-hover  btn btn-success exporttoexcel" OnClick="btnAllExport_Click" />

                    </div>
                    <div class="col-xs-12 text-right ">

                      <div class="row">
                            <div class="col-xs-3 form-group">
                                <asp:textbox id="txtdate" placeholder="Select Date" runat="server" autocomplete="off" cssclass="calender form-control"></asp:textbox>
                            </div>

                            <div class="col-xs-3 form-group">
                                <asp:DropDownList ID="ddlTimeSlot" class="form-control" runat="server">
                                <asp:ListItem Value="Select Time">Select Time</asp:ListItem>
                                <asp:ListItem Value="6 am - 7 am">6 am - 7 am</asp:ListItem>
                                <asp:ListItem Value="7 am - 8 am">7 am - 8 am</asp:ListItem>
                                <asp:ListItem Value="8 am - 9 am">8 am - 9 am</asp:ListItem>
                                <asp:ListItem Value="9 am - 10 am">9 am - 10 am</asp:ListItem>
                                <asp:ListItem Value="10 am - 11 am">10 am - 11 am</asp:ListItem>
                                <asp:ListItem Value="11 am - 12 pm">11 am - 12 pm</asp:ListItem>
                                <asp:ListItem Value="12 pm - 1 pm">12 pm - 1 pm</asp:ListItem>
                                <asp:ListItem Value="1 pm - 2 pm">1 pm - 2 pm</asp:ListItem>
                                <asp:ListItem Value="2 pm - 3 pm">2 pm - 3 pm</asp:ListItem>
                                <asp:ListItem Value="3 pm - 4 pm">3 pm - 4 pm</asp:ListItem>
                                <asp:ListItem Value="4 pm - 5 pm">4 pm - 5 pm</asp:ListItem>
                                <asp:ListItem Value="5 pm - 6 pm">5 pm - 6 pm</asp:ListItem>
                                <asp:ListItem Value="6 pm - 7 pm">6 pm - 7 pm</asp:ListItem>
                                <asp:ListItem Value="7 pm - 8 pm">7 pm - 8 pm</asp:ListItem>
                                <asp:ListItem Value="8 pm - 9 pm">8 pm - 9 pm</asp:ListItem>
                            </asp:DropDownList>
                            </div>

                                 <div class="col-xs-4 form-group">
                                <asp:Button ID="btnFilterExport" Text="Export to Excel Filter" runat="server" CssClass="fa-hover  btn btn-success exporttoexcel" OnClick="btnFilterExport_Click" /></div>

                           <%-- <div class="col-xs-3 form-group">
                                <asp:Button ID="btnFilterGrid" Text="Filter Data" runat="server" CssClass="fa-hover  btn btn-success exporttoexcel" OnClick="btnFilterGrid_Click1" />
                            </div>--%>

                  <%--          <div class="col-xs-3 form-group">
                                <asp:Button ID="btnAllExport" Text="Export to Excel" runat="server" CssClass="fa-hover  btn btn-success exporttoexcel" OnClick="btnAllExport_Click" />
                            </div>--%>
                        </div>

                    </div>

                </div>
                </div>
            <div class="clearfix"></div>

            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_content">
                            <uc1:MyMessageBox ID="MyMessageBox1" runat="server" />
                            <asp:GridView ID="gdView" runat="server" AutoGenerateColumns="False" OnPageIndexChanging="gdView_PageIndexChanging"
                                OnRowDeleting="gdView_RowDeleting" DataKeyNames="AppId"
                                class="table table-striped table-bordered" PageSize="10" AllowPaging="true">
                                <Columns>
                                    <asp:BoundField DataField="AppId" HeaderText="AppId" />
                                    <asp:BoundField DataField="Sr" HeaderText="Sr No." />
                                    <asp:BoundField DataField="Name" HeaderText="Name" />
                                       <asp:BoundField DataField="Mobile" HeaderText="Mobile" />
                                    <asp:BoundField DataField="EmailId" HeaderText="EmailId" />
                                 
                                    <asp:BoundField DataField="AdharCard" HeaderText="AdharCard" />
                                    <asp:BoundField DataField="Date" HeaderText="Date" />
                                     <asp:BoundField DataField="TimeSlot" HeaderText="TimeSlot" />
                   

                                 <%--   <asp:TemplateField ShowHeader="False">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="LinkButton1" CssClass="btn" runat="server" OnClientClick="return confirm('do you want to delete this record?');"
                                                CausesValidation="False" CommandName="Delete">
                            <i class="icon-trash"></i> Delete
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>--%>
                                </Columns>
                                <%-- <PagerStyle CssClass="pagination"></PagerStyle>--%>
                            </asp:GridView>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <%--  --%>
    </div>

        <script>
        $(function () {
            $('.calender').datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'dd-mm-yy'
            });
        });
    </script>

    <style>
        /* MODIFIED DATE PICKER */
        .ui-datepicker {
            width: 250px !important;
            padding: 2px !important;
            font-size: 14px !important;
        }

            .ui-datepicker table {
                margin: 0 !important;
            }

            .ui-datepicker .ui-widget-header {
                background: #04869a !important;
                /*color: #fff;*/
                font-weight: normal;
            }

        .ui-state-highlight, .ui-widget-content .ui-state-highlight, .ui-widget-header .ui-state-highlight {
            background: #04869a !important;
            color: #fff !important;
            border: 1px solid #04869a !important;
        }

        .ui-datepicker select.ui-datepicker-month, .ui-datepicker select.ui-datepicker-year {
            margin: 0px 4px !important;
            font-size: 13px !important;
        }

        .ui-state-active, .ui-widget-content .ui-state-active, .ui-widget-header .ui-state-active {
            background: #04869a !important;
            color: #fff !important;
            border: 1px solid #04869a !important;
        }
    </style>

</asp:Content>

