﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;
using System.Net.Mail;
using System.Net;
using iTextSharp.tool.xml;
using System.Globalization;

public partial class registration : System.Web.UI.Page
{
    Utility utility = new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            txtfullName.Text = string.Empty;
            txtAadharNo.Text = string.Empty;
            txtEmailId.Text = string.Empty;
            txtmobileNo.Text = string.Empty;
            ddlTimeSlot.SelectedItem.Text = "Select Time";

            txtVisitor1.Text = string.Empty;
            txtVisitor1_AadharNo.Text = string.Empty;
            txtVisitor2.Text = string.Empty;
            txtVisitor2_AadharNo.Text = string.Empty;
            txtVisitor3.Text = string.Empty;
            txtVisitor3_AadharNo.Text = string.Empty;

            var today = DateTime.Today;
            var tomorrow = today.AddDays(1).ToLongDateString();

            txtDate.Text = tomorrow;

            //txtDate.Text = "Sunday, October 10, 2021";

            Session["EPass"] = null;
        }
    }

    protected void btn_Submit_Click(object sender, EventArgs e)
    {
        using (SqlCommand cmd = new SqlCommand("Proc_Register_Applicant"))
        {
      
            cmd.CommandType = System.Data.CommandType.StoredProcedure;

            //if (!CheckEmail(txtEmailId.Text.ToString().Trim()))
            //{
                if (!CheckRegisterUserAdhaar(txtAadharNo.Text.ToString().Trim(),txtDate.ToString().Trim()))
                {

                    if (!CountVistors(txtDate.Text.ToString().Trim(), ddlTimeSlot.SelectedItem.Text.ToString().Trim()))
                {
                    cmd.Parameters.AddWithValue("@para", "add");
                    cmd.Parameters.AddWithValue("@FullName", txtfullName.Text.ToString().Trim());
                    cmd.Parameters.AddWithValue("@Mobile", txtmobileNo.Text.ToString().Trim());
                    cmd.Parameters.AddWithValue("@EmailId", txtEmailId.Text.ToString().Trim());
                    cmd.Parameters.AddWithValue("@AdharCard", txtAadharNo.Text.ToString().Trim());
                    cmd.Parameters.AddWithValue("@Date", txtDate.Text.ToString().Trim());
                    cmd.Parameters.AddWithValue("@TimeSlot", ddlTimeSlot.SelectedItem.Text.ToString().Trim());


                    if (txtVisitor1.Text.ToString().Trim() != "" || txtVisitor1.Text.ToString().Trim() != string.Empty)
                    {
                        if (!CheckAdhaarVisitor1(txtVisitor1_AadharNo.Text.ToString().Trim(),txtDate.ToString().Trim()))
                        {
                            
                        }
                        else
                        {
                            return;
                        }
                    }


                    if (txtVisitor2.Text.ToString().Trim() != "" || txtVisitor2.Text.ToString().Trim() != string.Empty)
                    {
                        if (!CheckAdhaarVisitor2(txtVisitor2_AadharNo.Text.ToString().Trim(),txtDate.ToString().Trim()))
                        {

                        }
                        else
                        {
                            return;
                        }
                    }

                    if (txtVisitor3.Text.ToString().Trim() != "" || txtVisitor3.Text.ToString().Trim() != string.Empty)
                    {
                        if (!CheckAdhaarVisitor3(txtVisitor3_AadharNo.Text.ToString().Trim(),txtDate.ToString().Trim()))
                        {

                        }
                        else
                        {
                            return;
                        }
                    }


                    if (utility.Execute(cmd))
                    {
                        DataTable dt = new DataTable();

                        dt = utility.Display("Execute Proc_Register_Applicant 'getAppIDByEmailId',0,'','','" + txtEmailId.Text.ToString().Trim() + "'");

                        if (dt.Rows.Count > 0)
                        {
                            if (txtVisitor1.Text.ToString().Trim() != "" || txtVisitor1.Text.ToString().Trim() != string.Empty)
                            {
                                using (SqlCommand cmd1 = new SqlCommand("Proc_Visitors"))
                                {

                                    cmd1.CommandType = System.Data.CommandType.StoredProcedure;
                                    cmd1.Parameters.AddWithValue("@para", "add");
                                    cmd1.Parameters.AddWithValue("@AppId", int.Parse(dt.Rows[0]["AppId"].ToString()));
                                    cmd1.Parameters.AddWithValue("@Visitor_FullName", txtVisitor1.Text.ToString().Trim());
                                    cmd1.Parameters.AddWithValue("@Visitor_AdhaarCard", txtVisitor1_AadharNo.Text.ToString().Trim());


                                    utility.Execute(cmd1);

                                }
                            }
                            if (txtVisitor2.Text.ToString().Trim() != "" || txtVisitor2.Text.ToString().Trim() != string.Empty)
                            {
                                using (SqlCommand cmd2 = new SqlCommand("Proc_Visitors"))
                                {

                                    cmd2.CommandType = System.Data.CommandType.StoredProcedure;
                                    cmd2.Parameters.AddWithValue("@para", "add");
                                    cmd2.Parameters.AddWithValue("@AppId", int.Parse(dt.Rows[0]["AppId"].ToString()));
                                    cmd2.Parameters.AddWithValue("@Visitor_FullName", txtVisitor2.Text.ToString().Trim());
                                    cmd2.Parameters.AddWithValue("@Visitor_AdhaarCard", txtVisitor2_AadharNo.Text.ToString().Trim());


                                    utility.Execute(cmd2);

                                }
                            }

                            if (txtVisitor3.Text.ToString().Trim() != "" || txtVisitor3.Text.ToString().Trim() != string.Empty)
                            {
                                using (SqlCommand cmd3 = new SqlCommand("Proc_Visitors"))
                                {

                                    cmd3.CommandType = System.Data.CommandType.StoredProcedure;
                                    cmd3.Parameters.AddWithValue("@para", "add");
                                    cmd3.Parameters.AddWithValue("@AppId", int.Parse(dt.Rows[0]["AppId"].ToString()));
                                    cmd3.Parameters.AddWithValue("@Visitor_FullName", txtVisitor3.Text.ToString().Trim());
                                    cmd3.Parameters.AddWithValue("@Visitor_AdhaarCard", txtVisitor3_AadharNo.Text.ToString().Trim());


                                    utility.Execute(cmd3);

                                }
                            }

                        }

                        string serialnumber = dt.Rows[0]["AppId"].ToString();

                     

                        bool response = SendMail(serialnumber);

                        //if (response)
                        //{
                        //    Response.Redirect("/thankyou.aspx");
                        //}
                        //else
                        //{
                        //    ScriptManager.RegisterStartupScript(this, GetType(), "alertMessage", "Please Enter Valid Email Address", true);
                        //}    

                        Response.Redirect("/thankyou.aspx");

                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "alertMessage", "Something went wrong, please try after sometime.", true);
                    }
                }

            }
            //}
               
           
 
        }
    }


    public bool SendMail(string EpassSrNo)
    {
        bool status = false;

        try
        {
            using (StringWriter sw = new StringWriter())
            {
                using (HtmlTextWriter hw = new HtmlTextWriter(sw))
                {

                    StringBuilder strbuild = new StringBuilder();

                    strbuild.Append("<table cellpadding='0' cellspacing='0' border='0' style='font-family:Arial, Helvetica, sans-serif;color:#333;font-size:16px;font-weight:normal;line-height:1.5;width:100%;max-width:1000px;border:1px solid #cccccc'>");
                    strbuild.Append("<thead>");
                    strbuild.Append("<tr>");
                    strbuild.Append("<th align='center' style='padding:10px 30px'>");
                    strbuild.Append("<table width='100%'>");
                    strbuild.Append("<tr>");
                    strbuild.Append("<td width='150'><img src='http://mahalakshmi-temple.com/images/logo.png'/></td>");
                    strbuild.Append("<td align='left'>");
                    strbuild.Append("<h1 style='font-size:20px;color:#cc0000;display:inline-block;margin-bottom:10px'>Shri Mahalakshmi Temple Trust, Mumbai</h1>");
                    strbuild.Append("<h2 style='color:#f96606;font-size:18px;margin:0'>Navratri Darshan 2021 E-pass</h2>");
                    strbuild.Append("</td>");
                    strbuild.Append("</tr>");
                    strbuild.Append("</table>");
                    strbuild.Append("</th>");
                    strbuild.Append("</tr>");
                    strbuild.Append("</thead>");
                    strbuild.Append("<tbody>");
                    strbuild.Append("<tr>");
                    strbuild.Append("<td align='center' style='padding:0px 30px'>");
                    strbuild.Append("<table width='100%' cellspacing='0' cellpadding='4' border='1' style='border:1px solid #aaa;border-collapse:collapse'>");
                    strbuild.Append("<tr style='background:#f3f3f3'>");
                    strbuild.Append("<td align='center'> E-pass AppId. <br/> <strong style='color:#cc0000'>" + EpassSrNo.ToString() + "</strong></td>");
                    strbuild.Append("<td align='center'>E-pass Date <br/> <strong style='color:#cc0000'>" + txtDate.Text.ToString() + "</strong></td>");
                    strbuild.Append("<td align='center'>Time Slot Selected<br/> <strong style='color:#cc0000'>" + ddlTimeSlot.SelectedItem.Text.ToString() + "</strong></td>");
                    strbuild.Append("</tr>");
                    strbuild.Append("</table>");
                    strbuild.Append("</td>");
                    strbuild.Append("</tr>");
                    strbuild.Append("<tr><td height='20px'></td></tr>");
                    strbuild.Append("<tr>");
                    strbuild.Append("<td align='left' style='padding:30px'>");
                    strbuild.Append("<p><strong style='color:#cc0000'>Visitor Details:</strong></p>");
                    strbuild.Append("<table width='100%' cellpadding='10' cellspacing='0' style='border-top:1px solid #666;border-bottom:1px solid #666;border-collapse:collapse;margin-bottom:50px'>");
                    strbuild.Append("<thead>");
                    strbuild.Append("<tr style='border-bottom:1px solid #666'>");
                    strbuild.Append("<th width='100'>Sr. No.</th>");
                    strbuild.Append("<th align='left'>Full Name</th>");
                    strbuild.Append("<th align='left'>Aadhar No.</th>");
                    strbuild.Append("</tr>");
                    strbuild.Append("</thead>");
                    strbuild.Append("<tbody>");
                    strbuild.Append("<tr>");
                    strbuild.Append("<td align='center'>1.</td>");
                    strbuild.Append("<td align='left'>" + txtfullName.Text.ToString() + "</td>");
                    strbuild.Append("<td align='left'>" + txtAadharNo.Text.ToString() + "</td>");
                    strbuild.Append("</tr>");
                    if (txtVisitor1.Text.ToString() != "")
                    {
                        strbuild.Append("<tr>");
                        strbuild.Append("<td align='center'>2.</td>");
                        strbuild.Append("<td align='left'>" + txtVisitor1.Text.ToString() + "</td>");
                        strbuild.Append("<td align='left'>" + txtVisitor1_AadharNo.Text.ToString() + "</td>");
                        strbuild.Append("</tr>");

                    }

                    if (txtVisitor2.Text.ToString() != "")
                    {
                        strbuild.Append("<tr>");
                        strbuild.Append("<td align='center'>3.</td>");
                        strbuild.Append("<td align='left'>" + txtVisitor2.Text.ToString() + "</td>");
                        strbuild.Append("<td align='left'>" + txtVisitor2_AadharNo.Text.ToString() + "</td>");
                        strbuild.Append("</tr>");
                    }

                    if (txtVisitor3.Text.ToString() != "")
                    {
                        strbuild.Append("<tr>");
                        strbuild.Append("<td align='center'>4.</td>");
                        strbuild.Append("<td align='left'>" + txtVisitor3.Text.ToString() + "</td>");
                        strbuild.Append("<td align='left'>" + txtVisitor3_AadharNo.Text.ToString() + "</td>");
                        strbuild.Append("</tr>");
                    }
                    strbuild.Append("</tbody>");
                    strbuild.Append("</table>");
                    strbuild.Append("</td>");
                    strbuild.Append("</tr>");
                    strbuild.Append("</tbody>");
                    strbuild.Append("<tfoot>");
                    strbuild.Append("<tr style='background:#f3f3f3'>");
                    strbuild.Append("<td align='left' style='padding:30px'>");
                    strbuild.Append("<p><strong>Note:</strong></p>");
                    strbuild.Append("<ul>");
                    strbuild.Append("<li>This E-pass is not transferable, you have to come on the date and time selected as per E-pass.</li>");
                    strbuild.Append("<li>Anyone not adhering to terms and conditions will not be allowed to enter.</li>");
                    strbuild.Append("<li>Kindly carry Aadhar Card of each visitor mentioned above during darshan.</li>");
                    strbuild.Append("<li>All the COVID-19 protocols needs to be followed.</li>");
                    strbuild.Append("</ul>");
                    strbuild.Append("</td>");
                    strbuild.Append("</tr>");
                    strbuild.Append("</tfoot>");
                    strbuild.Append("</table>");




                    StringReader sr = new StringReader(strbuild.ToString());

                    Document pdfDoc = new Document(PageSize.A4, 10f, 10f, 10f, 0f);



                    using (MemoryStream memoryStream = new MemoryStream())
                    {

                        StringBuilder sb = new StringBuilder();
                        sb.Append("Dear " + txtfullName.Text + ",");
                        sb.Append("<p>Thank you for registering for the <strong>'Navratri Darshan E-pass'.</strong></p>");
                        sb.Append("<p>We welcome you at Shri Mahalakshmi Mandir, with your E-pass attached in this mail. Kindly check the attachment for your E-pass.</p>");
                        sb.Append("<p><strong>Please Note:</strong></p>");
                        sb.Append("<ul>");
                        sb.Append("<li>This E-pass is not transferable, you have to come on the date and time selected as per E-pass.</li>");
                        sb.Append("<li>Anyone not adhering to terms and conditions will not be allowed to enter.</li>");
                        sb.Append("<li>Please bring your E-pass on your Phone or a Print-out along with Aadhar cards of all visitors during the darshan.</li>");
                        sb.Append("<li>All the COVID-19 protocols needs to be followed.</li>");
                        sb.Append("</ul>");
                        sb.Append("<p>Regards,</p>");
                        sb.Append("<p><strong>Shri Mahalakshmi Temple Trust,</strong></p>");
                        sb.Append("<p>B.D. Road, Mumbai - 400 026.</p>");

                        PdfWriter writer = PdfWriter.GetInstance(pdfDoc, memoryStream);
                        pdfDoc.Open();
                        //htmlparser.Parse(sr);
                        XMLWorkerHelper.GetInstance().ParseXHtml(writer, pdfDoc, sr);
                        pdfDoc.Close();

                    

                        byte[] bytes = memoryStream.ToArray();


                        if (!Directory.Exists(Server.MapPath("~/Files")))
                        {
                            Directory.CreateDirectory(Server.MapPath("~/Files"));
                        }

                        var myUniqueFileName = string.Format(@"{0}.txt", Guid.NewGuid());
                        string myfile = myUniqueFileName + "-Epass.pdf";
                        File.WriteAllBytes(Server.MapPath("~/Files/" + myfile), bytes);

                        Session["EPass"] = "~/Files/" + myfile;

                        //var path = Server.MapPath(@Session["EPass"].ToString());


                        //try {
                        //    using (FileStream fs = new FileStream(path, FileMode.Open, FileAccess.Read))
                        //    {

                       

                        //        SetStream(fs);


                        //    }

                        //}
                        //catch (Exception ex)
                        //{


                        //}

                        //var path = Server.MapPath(@Session["EPass"].ToString());

                        //using (FileStream fs = new FileStream(path, FileMode.Open, FileAccess.Read))
                        //{
                        //    SetStream(fs);


                        //}

                        //Response.Clear();
                        //Response.Buffer = true;
                        //Response.Charset = "";
                        //Response.Cache.SetCacheability(HttpCacheability.NoCache);
                        ////Response.ContentType = contentType;
                        //Response.AppendHeader("Content-Disposition", "attachment; filename=E-Pass.pdf");
                        //Response.BinaryWrite(bytes);
                        //Response.Flush();
                        //Response.End();


                        memoryStream.Close();

                        //MailMessage mm = new MailMessage("registration@mahalakshmi-temple.com", txtEmailId.Text.ToString());

                        //MailMessage mm = new MailMessage();
                        //mm.From = new MailAddress(ConfigurationManager.AppSettings["EmailUsername"].ToString(), "Mahalakshmi Mandir - Mumbai");
                        //mm.To.Add(new MailAddress(txtEmailId.Text.ToString()));
                        //mm.Subject = "Mahalakshmi Mandir Darshan E-pass";
                        //mm.Body = sb.ToString();
                        //mm.Attachments.Add(new Attachment(new MemoryStream(bytes), "E-Pass.pdf"));
                        //mm.IsBodyHtml = true;
                        //SmtpClient smtp = new SmtpClient();
                        //smtp.Host = ConfigurationManager.AppSettings["EmailHost"].ToString();
                        //smtp.EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSsl"].ToString());


                        //NetworkCredential NetworkCred = new NetworkCredential();
                        //NetworkCred.UserName = ConfigurationManager.AppSettings["EmailUsername"].ToString();
                        //NetworkCred.Password = ConfigurationManager.AppSettings["EmailPassword"].ToString();
                        //smtp.UseDefaultCredentials = false;
                        //smtp.Credentials = NetworkCred;
                        //smtp.Port = int.Parse(ConfigurationManager.AppSettings["EmailPort"].ToString());

                        //smtp.Send(mm);


                        //MailMessage mm = new MailMessage("rahul@kwebmaker.com", txtEmailId.Text.ToString());
                        //mm.Subject = "Mahalakshmi Mandir Darshan E-pass";
                        //mm.Body = sb.ToString();
                        //mm.Attachments.Add(new Attachment(new MemoryStream(bytes), "E-Pass.pdf"));
                        //mm.IsBodyHtml = true;
                        //SmtpClient smtp = new SmtpClient();
                        //smtp.Host = ConfigurationManager.AppSettings["EmailHost"].ToString();
                        //smtp.EnableSsl = false;
                        //NetworkCredential NetworkCred = new NetworkCredential();
                        //NetworkCred.UserName = ConfigurationManager.AppSettings["EmailUsername"].ToString();
                        //NetworkCred.Password = ConfigurationManager.AppSettings["EmailPassword"].ToString();
                        //smtp.UseDefaultCredentials = true;
                        //smtp.Credentials = NetworkCred;
                        //smtp.Port = 587;
                        //smtp.Send(mm);

                        status = true;

                    }
                }






            }
        }
        catch (Exception ex)
        {
            status = false;

        }
        return status;
    }


    //public bool CheckEmail(string useroremail)
    //{
    //    bool status;

    //    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["appcon"].ConnectionString);

    //    using (SqlCommand cmd = new SqlCommand("Proc_Register_Applicant", con))
    //    {
    //        con.Open();
    //        cmd.CommandType = CommandType.StoredProcedure;
    //        cmd.Parameters.AddWithValue("@para", "CheckEmailExist");
    //        cmd.Parameters.AddWithValue("@EmailId", useroremail.Trim());
    //        SqlDataReader dr = cmd.ExecuteReader();

    //        if (dr.HasRows)
    //        {
    //            lblStatus.Text = "Already registered with email id";
    //            lblStatus.Visible = true;
    //            status = true;
    //        }
    //        else
    //        {

    //            status = false;
    //        }

    //        con.Close();
    //    }

    //    return status;
    //}

    public bool CountVistors(string Date,string TimeSlot)
    {
        bool status=false;

        try
        {
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["appcon"].ConnectionString);

            using (SqlCommand cmd = new SqlCommand("Proc_Register_Applicant", con))
            {
                con.Open();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@para", "getTotalVisitorByDateTimeSlot");
                cmd.Parameters.AddWithValue("@Date", Date.Trim());
                cmd.Parameters.AddWithValue("@TimeSlot", TimeSlot.Trim());
                int count = (int)cmd.ExecuteScalar();

                if (count == 500 || count > 500)
                {
                    lblFulleTimeSlot.Text = "This slot is already full, Please select another Time slot.";
                    lblFulleTimeSlot.Visible = true;
                    status = true;
                }
                else if (txtVisitor1.Text != "" && txtVisitor2.Text != "" && txtVisitor3.Text != "")
                {
                    if (count == 497 || count > 497)
                    {
                        lblFulleTimeSlot.Text = "This slot is already full, Please select another Time slot.";
                        lblFulleTimeSlot.Visible = true;
                        status = true;
                    }
                }
                else if (txtVisitor1.Text != "" && txtVisitor2.Text != "")
                {
                    if (count == 498 || count > 498)
                    {
                        lblFulleTimeSlot.Text = "This slot is already full, Please select another Time slot.";
                        lblFulleTimeSlot.Visible = true;
                        status = true;
                    }
                }
                else if (txtVisitor1.Text != "")
                {
                    if (count == 499 || count > 499)
                    {
                        lblFulleTimeSlot.Text = "This slot is already full, Please select another Time slot.";
                        lblFulleTimeSlot.Visible = true;
                        status = true;
                    }
                }
                else
                {

                    //ddlTimeSlot.SelectedItem.Text.ToString() ="Select Time";
                    //ddlTimeSlot.SelectedItem.Text = "Select Time";
                    status = false;
                }




                con.Close();
            }
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "alertMessage", "Something went wrong, please try after sometime!!.", true);
            status = true;
        }
       

        return status;
    }



    public bool CheckRegisterUserAdhaar(string RegisterUserAdhaar,string Date)
    {
        bool status;

        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["appcon"].ConnectionString);

        using (SqlCommand cmd = new SqlCommand("Proc_Register_Applicant", con))
        {
            con.Open();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@para", "CheckAdhaarRegister_ApplicantExist");
            cmd.Parameters.AddWithValue("@AdharCard", RegisterUserAdhaar.Trim());
            cmd.Parameters.AddWithValue("@Date", Date);
            SqlDataReader dr = cmd.ExecuteReader();

            if (dr.HasRows || RegisterUserAdhaar==txtVisitor1_AadharNo.Text || RegisterUserAdhaar==txtVisitor2_AadharNo.Text || RegisterUserAdhaar == txtVisitor3_AadharNo.Text)
            {
                lblStatusAadharNo.Text = "aadhar number should be unique for all and not already registered.";
                lblStatusAadharNo.Visible = true;
                status = true;
            }
            else
            {
                lblStatusAadharNo.Visible = false;
                status = false;
            }

            con.Close();
        }

        return status;
    }

    public bool CheckAdhaarVisitor1(string Visitor1,string Date)
    {
        bool status;

        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["appcon"].ConnectionString);

        using (SqlCommand cmd = new SqlCommand("Proc_Register_Applicant", con))
        {
            con.Open();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@para", "CheckAdhaarRegister_ApplicantExist");
            cmd.Parameters.AddWithValue("@AdharCard", Visitor1.Trim());
            cmd.Parameters.AddWithValue("@Date", Date);
            SqlDataReader dr = cmd.ExecuteReader();

            if (dr.HasRows || Visitor1 == txtAadharNo.Text || Visitor1 == txtVisitor2_AadharNo.Text || Visitor1 == txtVisitor3_AadharNo.Text)
            {

                lblVisitor1Adhaar.Text = "aadhar number should be unique for all and not already registered.";
                lblVisitor1Adhaar.Visible = true;
                status = true;
            }
            else
            {
                lblVisitor1Adhaar.Visible = false;
                status = false;
            }

            con.Close();
        }

        return status;
    }



    public bool CheckAdhaarVisitor2(string Visitor2,string Date)
    {
        bool status;

        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["appcon"].ConnectionString);

        using (SqlCommand cmd = new SqlCommand("Proc_Register_Applicant", con))
        {
            con.Open();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@para", "CheckAdhaarRegister_ApplicantExist");
            cmd.Parameters.AddWithValue("@AdharCard", Visitor2.Trim());
            cmd.Parameters.AddWithValue("@Date", Date);
            SqlDataReader dr = cmd.ExecuteReader();

            if (dr.HasRows || Visitor2 == txtVisitor1_AadharNo.Text || Visitor2 == txtAadharNo.Text || Visitor2 == txtVisitor3_AadharNo.Text)
            {

                lblVisitor2Adhaar2.Text = "aadhar number should be unique for all and not already registered.";
                lblVisitor2Adhaar2.Visible = true;
                status = true;
            }
            else
            {
                lblVisitor2Adhaar2.Visible = false;
                status = false;
            }

            con.Close();
        }

        return status;
    }


    public bool CheckAdhaarVisitor3(string Visitor3,string Date)
    {
        bool status;

        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["appcon"].ConnectionString);

        using (SqlCommand cmd = new SqlCommand("Proc_Register_Applicant", con))
        {
            con.Open();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@para", "CheckAdhaarRegister_ApplicantExist");
            cmd.Parameters.AddWithValue("@AdharCard", Visitor3.Trim());
            cmd.Parameters.AddWithValue("@Date", Date);
            SqlDataReader dr = cmd.ExecuteReader();

            if (dr.HasRows || Visitor3 == txtVisitor1_AadharNo.Text || Visitor3 == txtAadharNo.Text || Visitor3 == txtVisitor2_AadharNo.Text)
            {

                lblVisitor3Adhaar3.Text = "aadhar number should be unique for all and not already registered.";
                lblVisitor3Adhaar3.Visible = true;
                status = true;
            }
            else
            {
                lblVisitor3Adhaar3.Visible = false;
                status = false;
            }

            con.Close();
        }

        return status;
    }



    //public bool CheckVisitorsAdhaar(string VisitorAdhaar)
    //{
    //    bool status;

    //    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["appcon"].ConnectionString);

    //    using (SqlCommand cmd = new SqlCommand("Proc_Visitors", con))
    //    {
    //        con.Open();
    //        cmd.CommandType = CommandType.StoredProcedure;
    //        cmd.Parameters.AddWithValue("@para", "CheckAdhaarVisitors");
    //        cmd.Parameters.AddWithValue("@AdharCard", VisitorAdhaar.Trim());
    //        SqlDataReader dr = cmd.ExecuteReader();

    //        if (dr.HasRows)
    //        {
    //            lblStatusAadharNo.Text = "Already registered with Adhaar no";
    //            lblStatusAadharNo.Visible = true;
    //            status = true;
    //        }
    //        else
    //        {

    //            status = false;
    //        }

    //        con.Close();
    //    }

    //    return status;
    //}

    private void SetStream(Stream stream)
    {
        byte[] bytes = new byte[(int)stream.Length];
        string fileName = "E-Pass.pdf";
        stream.Read(bytes, 0, (int)stream.Length);
        Response.Clear();
        Response.Buffer = true;
        Response.ContentType = "application/pdf";

        Response.AddHeader("content-disposition", "attachment; filename=" + fileName);
        Response.BinaryWrite(bytes);
        Response.End();

    }
}