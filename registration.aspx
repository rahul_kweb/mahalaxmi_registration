﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="registration.aspx.cs" Inherits="registration" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Shri Mahalakshmi Temple, Mumbai - Navratri Darshan Online Registration</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
    <link rel="stylesheet" href="css/style.css">
</head>
<body style="background-image: url(http://mahalakshmi-temple.com/images/bg.jpg)">
    <form id="form1" runat="server">
        <div class="pageWrapper">

            <header class="py-2">

                <div class="row">
                    <div class="col-auto">
                        <img src="http://mahalakshmi-temple.com/images/logo.png">
                    </div>
                    <div class="col align-self-center">
                        <h1 class="logoName mb-0">Shri Mahalakshmi Temple Trust, Mumbai</h1>
                    </div>
                </div>

            </header>

            <div class="container-lg formWrapper mb-4 p-2">

                <img src="http://mahalakshmi-temple.com/images/banner/banner3.jpg" class="w-100 mb-4">

                <h2 class="pageTitle text-center mb-4" style="color: #f96606">Navratri Darshan Online Registration</h2>

                <div class="formHolder px-2 px-sm-4 mx-auto">

                    <div class="row gx-lg-5">

                        <div class="col-lg-6 mb-4">
                            <label for="fullName" class="form-label mb-1">Full Name <sup>*</sup></label>
                            <asp:TextBox ID="txtfullName" class="form-control" runat="server" autocomplete="off"></asp:TextBox>
                            <%--<input type="text" class="form-control" id="fullName" placeholder="">--%>
                            <div id="ErrorName" class="invalid-feedback">Enter your Full Name</div>
                        </div>

                        <div class="col-lg-6 mb-4">
                            <label for="mobileNo" class="form-label mb-1">Mobile No. <sup>*</sup></label>
                            <asp:TextBox ID="txtmobileNo" class="form-control" autocomplete="off" runat="server" MinLength="10" MaxLength="10" onkeypress="return isNumberKey1(event)"></asp:TextBox>
                            <%--<input type="text" class="form-control" id="mobileNo" placeholder="">--%>
                            <div id="ErrorPhone" class="invalid-feedback">Enter your Mobile No.</div>
                            <div id="ErrorPhone1" class="invalid-feedback">Invalid Mobile No.</div>
                        </div>

                        <div class="col-lg-6 mb-4">
                           <%-- <label for="EmailId" class="form-label mb-1">Email id <sup>*</sup></label>--%>
                             <label for="EmailId" class="form-label mb-1">Email id</label>
                            <asp:TextBox ID="txtEmailId" class="form-control" runat="server" autocomplete="off"></asp:TextBox>
                            <%--<input type="email" class="form-control" id="EmailId" placeholder="">--%>
                            <%--<div class="invalid-feedback">Enter your Email id</div> --%>                      
                            <%--<small><span class="rounded-circle infoIcon">&#8505;</span> Your E-pass will be emailed to this email id</small>--%>
                            <small><span class="rounded-circle infoIcon">&#8505;</span> Email id is not mandatory, kindly download your E-pass after Submit.</small>
                             <div id="ErrorEmail" class="invalid-feedback">Enter your Email id.</div>
                              <div id="ErrorEmail1" class="invalid-feedback">Invalid email address.</div>
                              <asp:Label ID="lblStatus"  runat="server" Style="color:#dc3545;width: 100%;margin-top: .25rem;font-size: .875em;display:block;"></asp:Label>  
                        </div>

                        <div class="col-lg-6 mb-4">
                            <label for="AadharNo" class="form-label mb-1">Aadhar Card No. <sup>*</sup></label>
                            <asp:TextBox ID="txtAadharNo" class="form-control" runat="server" MinLength="12" MaxLength="12" autocomplete="off" onkeypress="return isNumberKey1(event)"></asp:TextBox>
                            <%--  <input type="text" class="form-control" id="AadharNo" placeholder="">--%>                          
                            <small><span class="rounded-circle infoIcon">&#8505;</span> Carry your Aadhar Card for validation at the Mandir</small>
                            <div id="ErrorAdhaar" class="invalid-feedback">Enter your Aadhar Card No.</div>
                              <div id="ErrorAdhaar1" class="invalid-feedback">Enter 12 digit aadhar card No.</div>
                               <asp:Label ID="lblStatusAadharNo"  runat="server" Style="color:#dc3545;width: 100%;margin-top: .25rem;font-size: .875em;display:block;"></asp:Label>  
                        </div>

                        <div class="col-12 mb-4">
                            <button class="btn btn-success" type="button" data-bs-toggle="collapse" data-bs-target="#addpeople" aria-expanded="false" aria-controls="addpeople">
                                <strong>+</strong> Add more people</button>
                        </div>

                        <div class="col-12 collapse" id="addpeople">
                            <p class="text-danger">Maximum 3 more people will be allowed in 1 pass, please fill below details if additional people joining you</p>

                            <div class="row gx-lg-5">
                                <div class="col-12"><strong style="color: #2E6401">Additional Visitor 1</strong></div>

                                <div class="col-lg-6 mb-4">
                                    <label for="fullName_Add1" class="form-label mb-1">Full Name </label>
                                    <%-- <input type="text" class="form-control" id="fullName_Add1" placeholder="">--%>
                                    <asp:TextBox ID="txtVisitor1" class="form-control" autocomplete="off" runat="server"></asp:TextBox>
                                    <div class="invalid-feedback">Enter your Full Name</div>
                                </div>

                                <div class="col-lg-6 mb-4">
                                    <label for="AadharNo_Add1" class="form-label mb-1">Aadhar Card No. </label>
                                    <asp:TextBox ID="txtVisitor1_AadharNo" class="form-control" autocomplete="off" runat="server" MinLength="12" MaxLength="12" onkeypress="return isNumberKey1(event)"></asp:TextBox>
                                     <div id="ErrorVisitor1Adhaar" class="invalid-feedback">Enter your Aadhar Card No.</div>
                                    <div id="ErrorVisitor1Adhaar1" class="invalid-feedback">Enter 12 digit aadhar card No.</div>
                                     <asp:Label ID="lblVisitor1Adhaar"  runat="server" Style="color:#dc3545;width: 100%;margin-top: .25rem;font-size: .875em;display:block;"></asp:Label>
                                    <%--<input type="text" class="form-control" id="AadharNo_Add1" placeholder="">--%>
                                    <%--<div class="invalid-feedback">Enter Aadhar Card No.</div>--%>
                                </div>

                                <div class="col-12"><strong style="color: #2E6401">Additional Visitor 2</strong></div>

                                <div class="col-lg-6 mb-4">
                                    <label for="fullName_Add2" class="form-label mb-1">Full Name </label>
                                    <asp:TextBox ID="txtVisitor2" class="form-control" autocomplete="off" runat="server"></asp:TextBox>
                                    <%--<input type="text" class="form-control" id="fullName_Add2" placeholder="">--%>
                                    <div class="invalid-feedback">Enter your Full Name</div>
                                </div>

                                <div class="col-lg-6 mb-4">
                                    <label for="AadharNo_Add2" class="form-label mb-1">Aadhar Card No. </label>
                                    <%--<input type="text" class="form-control" id="AadharNo_Add2" placeholder="">--%>
                                    <asp:TextBox ID="txtVisitor2_AadharNo" class="form-control" autocomplete="off" runat="server" MinLength="12" MaxLength="12" onkeypress="return isNumberKey1(event)"></asp:TextBox>
                                 <%--   <div class="invalid-feedback">Enter Aadhar Card No.</div>--%>
                                      <div id="ErrorVisitor2Adhaar" class="invalid-feedback">Enter your Aadhar Card No.</div>
                                    <div id="ErrorVisitor2Adhaar2" class="invalid-feedback">Enter 12 digit aadhar card No.</div>
                                    <asp:Label ID="lblVisitor2Adhaar2"  runat="server" Style="color:#dc3545;width: 100%;margin-top: .25rem;font-size: .875em;display:block;"></asp:Label>
                                </div>
                                <div class="col-12"><strong style="color: #2E6401">Additional Visitor 3</strong></div>

                                <div class="col-lg-6 mb-4">
                                    <label for="fullName_Add3" class="form-label mb-1">Full Name </label>
                                    <asp:TextBox ID="txtVisitor3" class="form-control" autocomplete="off" runat="server"></asp:TextBox>
                                    <%--<input type="text" class="form-control" id="fullName_Add3" placeholder="">--%>
                                    <div class="invalid-feedback">Enter your Full Name</div>
                                </div>

                                <div class="col-lg-6 mb-4">
                                    <label for="AadharNo_Add3" class="form-label mb-1">Aadhar Card No. </label>
                                    <asp:TextBox ID="txtVisitor3_AadharNo" class="form-control" autocomplete="off" runat="server" MinLength="12" MaxLength="12" onkeypress="return isNumberKey1(event)"></asp:TextBox>
                                    <%--<input type="text" class="form-control" id="AadharNo_Add3" placeholder="">--%>
                                    <%--<div class="invalid-feedback">Enter Aadhar Card No.</div>--%>
                                    <div id="ErrorVisitor3Adhaar" class="invalid-feedback">Enter your Aadhar Card No.</div>
                                    <div id="ErrorVisitor3Adhaar3" class="invalid-feedback">Enter 12 digit aadhar card No.</div>
                                      <asp:Label ID="lblVisitor3Adhaar3"  runat="server" Style="color:#dc3545;width: 100%;margin-top: .25rem;font-size: .875em;display:block;"></asp:Label>
                                </div>

                            </div>

                        </div>

                        <div class="col-lg-6 mb-4">
                            <label for="selectDate" class="form-label mb-1">E-pass issued for Date </label>
                            <asp:TextBox ID="txtDate" class="form-control" runat="server" ReadOnly="true"></asp:TextBox>
                            <%--<input type="text" class="form-control" id="" placeholder="" value="Thursday, 7 Oct 2021" readonly>--%>
                        </div>

                        <div class="col-lg-6 mb-4">
                            <label for="selectTime" class="form-label mb-1">Select Time <sup>*</sup></label>
                            <asp:DropDownList ID="ddlTimeSlot" class="form-select" runat="server">
                                <asp:ListItem Value="Select Time">Select Time</asp:ListItem>
                                <asp:ListItem Value="6 am - 7 am">6 am - 7 am</asp:ListItem>
                                <asp:ListItem Value="7 am - 8 am">7 am - 8 am</asp:ListItem>
                                <asp:ListItem Value="8 am - 9 am">8 am - 9 am</asp:ListItem>
                                <asp:ListItem Value="9 am - 10 am">9 am - 10 am</asp:ListItem>
                                <asp:ListItem Value="10 am - 11 am">10 am - 11 am</asp:ListItem>
                                <asp:ListItem Value="11 am - 12 pm">11 am - 12 pm</asp:ListItem>
                                <asp:ListItem Value="12 pm - 1 pm">12 pm - 1 pm</asp:ListItem>
                                <asp:ListItem Value="1 pm - 2 pm">1 pm - 2 pm</asp:ListItem>
                                <asp:ListItem Value="2 pm - 3 pm">2 pm - 3 pm</asp:ListItem>
                                <asp:ListItem Value="3 pm - 4 pm">3 pm - 4 pm</asp:ListItem>
                                <asp:ListItem Value="4 pm - 5 pm">4 pm - 5 pm</asp:ListItem>
                                <asp:ListItem Value="5 pm - 6 pm">5 pm - 6 pm</asp:ListItem>
                                <asp:ListItem Value="6 pm - 7 pm">6 pm - 7 pm</asp:ListItem>
                                <asp:ListItem Value="7 pm - 8 pm">7 pm - 8 pm</asp:ListItem>
                                <asp:ListItem Value="8 pm - 9 pm">8 pm - 9 pm</asp:ListItem>
                            </asp:DropDownList>
                            <%--  <select class="form-select" aria-label="selectTime">
						  <option>6 am - 7 am</option>
						  <option>7 am - 8 am</option>
						  <option>8 am - 9 am</option>
						  <option>9 am - 10 am</option>
						  <option>10 am - 11 am</option>
						  <option>11 am - 12 pm</option>
						  <option>12 pm - 1 pm</option>
						  <option>1 pm - 2 pm</option>
						  <option>2 pm - 3 pm</option>
						  <option>3 pm - 4 pm</option>
						  <option>4 pm - 5 pm</option>
						  <option>5 pm - 6 pm</option>
						  <option>6 pm - 7 pm</option>
						  <option>7 pm - 8 pm</option>
						  <option>8 pm - 9 pm</option>
						</select>--%>
                            <div class="invalid-feedback" id="ErrorTimeSlot">Select a Time slot</div>
                            <asp:Label ID="lblFulleTimeSlot"  runat="server" Style="color:#dc3545;width: 100%;margin-top: .25rem;font-size: .875em;"></asp:Label>  
                        </div>

                        <div class="col-12 mb-4">
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" value="" id="acceptTC" checked>
                                <label class="form-check-label" for="acceptTC">
                                    I have read and accept to the <a href="#" role="button" data-bs-toggle="modal" data-bs-target="#termsConditions">terms & Conditions</a>
                                </label>
                                <div id="errorcheck" class="invalid-feedback">Check the checkbox for terms and conditions</div>
                            </div>
                        </div>



                        <div class="col-12 mb-4">
                            <asp:Button ID="btn_Submit" CssClass="btn submitBtn" Text="Submit" runat="server" OnClientClick="javascript:return Checkfields();" OnClick="btn_Submit_Click"  />
                            <%--<button class="btn submitBtn" OnClientClick="javascript:return Checkfields();" type="submit">Submit</button>--%>
                        </div>

                        
                         <div class="col-lg-6 mb-5">
                              <label for="selectDate" class="form-label mb-1"><strong>Kindly download your E-pass after Submit.</strong></label>
                             </div>
                    </div>


                </div>

            </div>

            <footer>
                <div class="row text-center text-white mb-3">
                    <div class="col-lg-6 text-lg-start">Copyright &copy; 2021 Shri Mahalakshmi Temple Trust.</div>
                  <%--  <div class="col-lg-6 text-lg-end">Designed, developed & maintained by :	<a href="https://www.kwebmaker.com" target="_blank">Kwebmaker&trade;</a></div>--%>
                </div>
            </footer>

        </div>



        <div class="modal fade" id="termsConditions" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-hidden="true">
            <div class="modal-dialog modal-dialog-scrollable">
                <div class="modal-content">
                    <div class="modal-header bg-danger">
                        <h5 class="modal-title text-white">Terms & Conditions<br>
                            For online booking of Mukhdarshan</h5>
                    </div>
                    <div class="modal-body">

                        <ul class="tcList">
                            <li>Kindly wear the Mask when you come for Mataji Darshan. No Entry without Mask.</li>
                            <li>Keep Physical distance of at least of 3 feet from others, avoid crowds and close contact.</li>
                            <li>You should not be COVID-19 positive.</li>
                            <li>You should be vaccinated.</li>
                            <li>Age should be between 11 to 65 years, pregnant women not allowed.</li>
                            <li>No Offerings will be physically allowed.</li>
                            <li>You should be adhere to date, time and slot selected.</li>
                            <li>Kindly carry Aadhar Card during darshan.</li>
                        </ul>

                    </div>
                    <div class="modal-footer justify-content-center">
                        <button type="button" class="btn submitBtn" data-bs-dismiss="modal">I Agree</button>
                    </div>
                </div>
            </div>
        </div>

        <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-/bQdsTh/da6pkI1MST/rWKFNjaCP5gBSY4sEBT38Q/9RBh9AH40zEOg7Hlq2THRZ" crossorigin="anonymous"></script>
        <script type="text/javascript">
            $(window).on('load', function () {
                $('#termsConditions').modal('show', 'handleUpdate');
            });
   
           
        </script>

        <script>

      function isNumberKey1(evt) {

        var charCode = (evt.which) ? evt.which : event.keyCode;
       <%-- var textBox = document.getElementById('<%=txtmobileNo.ClientID%>').value.length;--%>
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

        return true;
      }

 function Checkfields() {
     debugger;
     var Name = $('#<%=txtfullName.ClientID%>').val();
     var Phone = $('#<%=txtmobileNo.ClientID%>').val();
     var Email = $('#<%=txtEmailId.ClientID%>').val();
     var Adhaar = $('#<%=txtAadharNo.ClientID%>').val();
     var TimeSlot = $('#<%=ddlTimeSlot.ClientID%>').val();


     var NameVisitor1 = $('#<%=txtVisitor1.ClientID%>').val();
     var NameVisitor2 = $('#<%=txtVisitor2.ClientID%>').val();
     var NameVisitor3 = $('#<%=txtVisitor3.ClientID%>').val();

     var AdhaarVisitor1 = $('#<%=txtVisitor1_AadharNo.ClientID%>').val();
     var AdhaarVisitor2 = $('#<%=txtVisitor2_AadharNo.ClientID%>').val();
     var AdhaarVisitor3 = $('#<%=txtVisitor3_AadharNo.ClientID%>').val();

            var Blank = false;
            var message = "";

            if (Name == '') {
                $('#ErrorName').css('display', 'block');
                Blank = true;
            }
            else {
                $('#ErrorName').css('display', 'none');

            }


            if (Phone == '') {
                $('#ErrorPhone').css('display', 'block');
                $('#ErrorPhone1').css('display', 'none');
                Blank = true;
            }
            else {
                var ValidPhone = /^\d{10}$/;

                if (!ValidPhone.test(Phone)) {
                    $('#ErrorPhone').css('display', 'none');
                    $('#ErrorPhone1').css('display', 'block');
                    Blank = true;
                }
                else {
                    $('#ErrorPhone1').css('display', 'none');


                }
                $('#ErrorPhone').css('display', 'none');


            }


            //if (Email == '') {
            //    $('#ErrorEmail').css('display', 'block');
            //    $('#ErrorEmail1').css('display', 'none');
            //    Blank = true;
            //}
            //else {
            //    var EmailText = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;

            //    if (!EmailText.test(Email)) {
            //        $('#ErrorEmail').css('display', 'none');
            //        $('#ErrorEmail1').css('display', 'block');
            //        Blank = true;
            //    }
            //    else {
            //        $('#ErrorEmail1').css('display', 'none');


            //    }
            //    $('#ErrorEmail').css('display', 'none');


            //}


            if (Adhaar == '') {
                $('#ErrorAdhaar').css('display', 'block');
                $('#ErrorAdhaar1').css('display', 'none');
                Blank = true;
            }
            else {
                var ValidAdhaar = /^\d{12}$/;

                if (!ValidAdhaar.test(Adhaar)) {
                    $('#ErrorAdhaar').css('display', 'none');
                    $('#ErrorAdhaar1').css('display', 'block');
                    Blank = true;
                }
                else {
                    $('#ErrorAdhaar1').css('display', 'none');


                }
                $('#ErrorAdhaar').css('display', 'none');


            }

            if (TimeSlot == 'Select Time') {
                $('#ErrorTimeSlot').css('display', 'block');
                Blank = true;
            }
            else {
                $('#ErrorTimeSlot').css('display', 'none');

            }


     // Validation for visitor

            if (NameVisitor1 != '') {
                if (AdhaarVisitor1 == '') {
                    $('#ErrorVisitor1Adhaar').css('display', 'block');
                    $('#ErrorVisitor1Adhaar1').css('display', 'none');
                    Blank = true;
                }
                else {
                    var ValidAdhaar = /^\d{12}$/;

                    if (!ValidAdhaar.test(AdhaarVisitor1)) {
                        $('#ErrorVisitor1Adhaar').css('display', 'none');
                        $('#ErrorVisitor1Adhaar1').css('display', 'block');
                        Blank = true;
                    }
                    else {
                        $('#ErrorVisitor1Adhaar1').css('display', 'none');


                    }
                    $('#ErrorVisitor1Adhaar').css('display', 'none');


                }
            }

            if (NameVisitor2 != '') {
                if (AdhaarVisitor2 == '') {
                    $('#ErrorVisitor2Adhaar').css('display', 'block');
                    $('#ErrorVisitor2Adhaar2').css('display', 'none');
                    Blank = true;
                }
                else {
                    var ValidAdhaar = /^\d{12}$/;

                    if (!ValidAdhaar.test(AdhaarVisitor2)) {
                        $('#ErrorVisitor2Adhaar').css('display', 'none');
                        $('#ErrorVisitor2Adhaar2').css('display', 'block');
                        Blank = true;
                    }
                    else {
                        $('#ErrorVisitor2Adhaar2').css('display', 'none');


                    }
                    $('#ErrorVisitor2Adhaar').css('display', 'none');


                }
            }


            if (NameVisitor3 != '') {
                if (AdhaarVisitor3 == '') {
                    $('#ErrorVisitor3Adhaar').css('display', 'block');
                    $('#ErrorVisitor3Adhaar3').css('display', 'none');
                    Blank = true;
                }
                else {
                    var ValidAdhaar = /^\d{12}$/;

                    if (!ValidAdhaar.test(AdhaarVisitor3)) {
                        $('#ErrorVisitor3Adhaar').css('display', 'none');
                        $('#ErrorVisitor3Adhaar3').css('display', 'block');
                        Blank = true;
                    }
                    else {
                        $('#ErrorVisitor3Adhaar3').css('display', 'none');


                    }
                    $('#ErrorVisitor3Adhaar').css('display', 'none');


                }
            }
     

            if (acceptTC.checked == false) {
                $('#errorcheck').css('display', 'block');
                Blank = true;
            }
            else {
                $('#errorcheck').css('display', 'none');
            }


            if (Blank) {

                return false;

            }
            else {
                return true;
            }
        }
</script>
    </form>
</body>
</html>
