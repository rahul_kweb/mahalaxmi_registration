﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <center><table cellpadding='0' cellspacing='0' border='0' style='font-family:Arial, Helvetica, sans-serif;color:#333;font-size:16px;font-weight:normal;line-height:1.5;width:100%;max-width:1000px;border:1px solid #cccccc'><thead>
        <tr>
            <th align='center' style='padding:10px 30px'><table width='100%'>
                <tr>
                    <td width='150'>
                <img src='http://mahalakshmi-temple.com/images/logo.png'/>

                                                                                </td>
                <td align='left'><h1 style='font-size:20px;color:#cc0000;display:inline-block;margin-bottom:10px'>Shri Mahalakshmi Temple Trust, Mumbai</h1>
                    <h2 style='color:#f96606;font-size:18px;margin:0'>Navratri Darshan 2021 E-pass</h2></td>

                </tr></table></th></tr></thead><tbody><tr><td align='center' style='padding:0px 30px'>
                    <table width='100%' cellspacing='0' cellpadding='4' border='1' style='border:1px solid #aaa;border-collapse:collapse'><tr style='background:#f3f3f3'>
                        <td align='center'>E-pass Date <br/> <strong style='color:#cc0000'>October 06, 2021</strong></td><td align='center'>Time Slot Selected<br/> 
                            <strong style='color:#cc0000'>6 am - 7 am</strong></td></tr></table></td></tr><tr><td style='padding:0px 30px'><p><strong style='color:#cc0000'>Visitor Details:</strong>

                                                                                                                                           </p><table width='100%' cellpadding='10' cellspacing='0' style='border-top:1px solid #666;border-bottom:1px solid #666;border-collapse:collapse;margin-bottom:50px'><thead>
                                                                                                                                               <tr style='border-bottom:1px solid #666'>
                                                                                                                                               <th width='100'>Sr. No.</th><th align='left'>Full Name</th><th align='left'>Aadhar No.</th></tr></thead><tbody><tr>
                                                                                                                                                   <td align='center'>1.</td><td>rahul</td><td>123412341234</td></tr></tbody></table></td></tr></tbody><tfoot bgcolor='#f3f3f3'><tr><td style='padding:30px'><p><strong>Note:</strong></p><ul><li>This E-pass is not transferable, you have to come on the date and time selected as per E-pass.</li><li>Anyone not adhering to terms and conditions will not be allowed to enter.</li><li>Kindly carry Aadhar Card of each visitor mentioned above during darshan.</li><li>All the COVID-19 protocols needs to be followed.</li></ul></td></tr></tfoot></table></center>

</body>
</html>
