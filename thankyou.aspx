﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="thankyou.aspx.cs" Inherits="thankyou" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    	<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>Shri Mahalakshmi Temple, Mumbai - Navratri Darshan Online Registration</title>
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">	
	<link rel="stylesheet" href="css/style.css">
</head>
  <body style="background-image:url(http://mahalakshmi-temple.com/images/bg.jpg)">
    <form id="form1" runat="server">
	<div class="pageWrapper">

		<header class="py-2">

			<div class="row">
				<div class="col-auto"><img src="http://mahalakshmi-temple.com/images/logo.png"></div>
				<div class="col align-self-center"><h1 class="logoName mb-0">Shri Mahalakshmi Temple Trust, Mumbai</h1></div>
			</div>
			
		</header>

		<div class="container-lg formWrapper mb-4 p-2">

			<img src="http://mahalakshmi-temple.com/images/banner/banner3.jpg" class="w-100 mb-4">

			<h2 class="pageTitle text-center mb-4" style="color:#f96606">Thank you for Registering for Navratri Darshan</h2>

            <asp:Button ID="DownloadEpass" Text="CLICK HERE TO DOWNLOAD YOUR E-PASS" Style="width:auto" CssClass="btn submitBtn" runat="server" OnClick="DownloadEpass_Click" />

			<div class="formHolder px-2 px-sm-4 mx-auto mb-5">

				<%--<p>Your E-pass has been emailed to you or download from here. Please bring your E-pass on your Phone or a Print-out along with Aadhar cards of all visitors.</p>--%>

                <p><strong>Please download yor E-pass from here.</strong> Please bring your E-pass on your Phone or a Print-out along with Aadhar cards of all visitors.</p>

				<p><strong>Please note - pass is not transferable, you have to come on the date and time selected as per E-pass.<br> Anyone not adhering to terms and conditions will not be allowed to enter.</strong></p>

				<p>Due to Covid guidelines :-<br>
					&bull; People under 10 years of age and above 65 years of age are not allowed.<br>
					&bull; Pregnant women are not allowed.
				</p>

                <p style="color:#dc3545;"><strong>If you do not see the Epass in your inbox, pls check your spam or junk folder</strong></p>

                        
			</div>

		</div>

		<footer>
			<div class="row text-center text-white mb-3">
				<div class="col-lg-6 text-lg-start">Copyright &copy; 2021 Shri Mahalakshmi Temple Trust.</div>
				<%--<div class="col-lg-6 text-lg-end">Designed, developed & maintained by :	<a href="https://www.kwebmaker.com" target="_blank">Kwebmaker&trade;</a></div>--%>
			</div>
		</footer>

	</div>

	<script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-/bQdsTh/da6pkI1MST/rWKFNjaCP5gBSY4sEBT38Q/9RBh9AH40zEOg7Hlq2THRZ" crossorigin="anonymous"></script>

    </form>
</body>
</html>
