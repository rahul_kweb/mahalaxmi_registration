﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class thankyou : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void DownloadEpass_Click(object sender, EventArgs e)
    {
        //string path = Session["EPass"].ToString();

        //string getpath = Session["EPass"].ToString();
        //string path = Server.UrlDecode(@getpath);

        var path = Server.MapPath(@Session["EPass"].ToString());


        using (FileStream fs = new FileStream(path, FileMode.Open, FileAccess.Read))
        {
            SetStream(fs);
        }

        Session["EPass"] = null;


    }
    private void SetStream(Stream stream)
    {
        byte[] bytes = new byte[(int)stream.Length];
        string fileName = "E-Pass.pdf";
        stream.Read(bytes, 0, (int)stream.Length);
        Response.Clear();
        Response.Buffer = true;
        Response.ContentType = "application/pdf";

        Response.AddHeader("content-disposition", "attachment; filename=" + fileName);
        Response.BinaryWrite(bytes);
        Response.End();
    }

}