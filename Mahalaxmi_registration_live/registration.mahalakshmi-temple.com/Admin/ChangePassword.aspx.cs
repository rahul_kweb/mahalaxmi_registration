﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_ChangePassword : AdminPage
{
    Utility utility=new Utility();
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        using (SqlCommand cmd = new SqlCommand("Proc_SuperAdminLogin"))
        {
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@PARA", "CHANGE_PASSWORD");
            cmd.Parameters.AddWithValue("@USERNAME", Session[AppKeys.SESSION_ADMIN_USERNAME_KEY].ToString());
            cmd.Parameters.AddWithValue("@PASSWORD",txtOldPassword.Text.ToString().Trim());
            cmd.Parameters.AddWithValue("@NEWPASSWORD",txtNewPassword.Text.ToString().Trim());
            DataTable dt = new DataTable();
            dt = utility.Display(cmd);
            if (dt.Rows[0]["update"].ToString() == "correct")
            {
                lblstatus.Visible = true;
                lblstatus.Text = "Your Password has been changed Successfully!";
            }
            else
            {
                lblstatus.Visible = true;
                lblstatus.Text = "Sorry ! Password Could not be changed Successfully!";
            }
           
        }
        
    }

}
